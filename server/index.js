// console.log("Implement servermu disini yak 😝!");
// const http = require("http");
// const fs = require("fs");
// const path = require("path");
// const { PORT = 8000 } = process.env;
// const PUBLIC_DIRECTORY = path.join(__dirname, "public");

// function getHTML(htmlFileName) {
//     const htmlFile = path.join(PUBLIC_DIRECTORY, htmlFileName);
//     const html = fs.readFileSync(htmlFile, "utf-8");
//     return html;
// }

// function toJSON(value) {
//     return JSON.stringify(value);
// }

// function onRequest(req, res) {
//     switch (req.url) {
//         case "/":
//             res.writeHead(200);
//             res.end(getHTML("index.html"));
//             return;
//         case "/about":
//             res.writeHead(200);
//             res.end(getHTML("about.html"));
//             return;
//         case "/data":
//             res.setHeader("Content-Type", "application/json");
//             res.writeHead(200);
//             res.end(responseJSON);
//             return;
//         default:
//             res.writeHead(200);
//             res.end(getHTML("404.html"));
//             return;
//     }
// }

// const server = http.createServer(onRequest);

// server.listen(PORT, "127.0.0.1", () => {
//     console.log("server run");
// });

// const http = require("http");
// const url = require("url");
// const fs = require("fs");
// const path = require("path");

// const hostname = "127.0.0.1";
// const port = 8000;

// const pathServer = path.join(__dirname, "server");
// const pathPublic = path.join(__dirname, "public");

// const server = http.createServer((req, res) => {
//     console.log(pathPublic);
//     let objStrParams;
//     if (Object.keys(url.parse(req.url, true).query).length != 0) {
//         objStrParams = url.parse(req.url, true).query;
//     }
//     console.log(objStrParams);
//     if (objStrParams != undefined) {
//         let url = req.url.split("?")[0];
//         console.log(url);
//         if (url == `/images`) {
//             res.statusCode = 200;
//             res.setHeader("Content-Type", "image");
//             console.log(path.join(pathServer, `img/${objStrParams.src}`));
//             res.end(fs.readFileSync(path.join(pathServer, `img/${objStrParams.src}`)));
//         } else if (url == `/scripts`) {
//             res.statusCode = 200;
//             res.setHeader("Content-Type", "text/javscript");
//             res.end(fs.readFileSync(path.join(pathServer, `scripts/${objStrParams.src}`)));
//         }
//     } else {
//         switch (req.url) {
//             case "/":
//                 res.statusCode = 200;
//                 res.setHeader("Content-Type", "text/html");
//                 res.end(fs.readFileSync(path.join(pathPublic, "index.html")));
//                 break;

//             default:
//                 res.statusCode = 200;
//                 res.setHeader("Content-Type", "text/html");
//                 res.end("404");
//                 break;
//         }
//     }
// });

// server.listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
// });

const http = require("http");
const fs = require("fs");

const port = 8000;

function onRequest(req, res) {
    switch (req.url) {
        case "/":
            res.writeHead(200);
            req.url = "index.html";
            break;
        case "/cars":
            res.writeHead(200);
            req.url = "cars.html";
            break;
    }

    let path = "public/" + req.url;
    fs.readFile(path, (err, data) => {
        res.writeHead(200);
        res.end(data);
    });
}
const server = http.createServer(onRequest);

server.listen(port, "localhost", () => {
    console.log("Server Sudah Berjalan");
});