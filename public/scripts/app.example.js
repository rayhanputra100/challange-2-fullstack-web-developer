class App {
    constructor() {
        this.clearButton = document.getElementById("clear-btn");
        this.loadButton = document.getElementById("load-btn");
        this.carContainerElement = document.getElementById("cars-container");
        this.tipeDriver = document.getElementById("supir");
        this.time = document.getElementById("time");
        this.date = document.getElementById("date");
        this.penumpang = document.getElementById("penumpang");
    }

    async init() {
        await this.load();

        // Register click listener
        this.clearButton.onclick = this.clear;
        this.loadButton.onclick = this.run;
    }

    run = () => {
        // auto delete
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }

        // Filter date time
        let date = this.date.value;
        let time = this.time.value;
        let dateTime = date + "T" + time;
        let formDate = Date.parse(dateTime);
        // Filter penumpang
        let penumpang = this.penumpang.value;

        // Filter tipe driver
        const driver = this.tipeDriver.value == "true" ? true : false;
        console.log(driver);
        console.log(this.tipeDriver);

        Car.list.forEach((car) => {
            // if (car.available == driver && Date.parse(car.availableAt) >= formDate && car.capacity >= this.penumpang)
            if (car.available == driver && Date.parse(car.availableAt) >= formDate && car.capacity >= penumpang) {
                const node = document.createElement("div");
                node.className = "col-md-3";
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
            }
        });
    };

    async load() {
        const cars = await Binar.listCars();
        Car.init(cars);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };
}